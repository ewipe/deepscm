#!/bin/sh
### General options
### –- specify queue --
#BSUB -q gpua40
### -- set the job Name --
#BSUB -J deepscmtrain
### -- ask for number of cores (default: 1) --
#BSUB -n 8
### -- Select the resources: 1 gpu in exclusive process mode --
#BSUB -gpu "num=1:mode=exclusive_process"
###BSUB -R "select[gpu24gb]", we uncomment this so we simple use the first available GPU
### -- set walltime limit: hh:mm --  maximum 24 hours for GPU-queues right now
#BSUB -W 24:00
# request 5GB of system-memory
#BSUB -R "rusage[mem=4GB]"
### -- set the email address --
# please uncomment the following line and put in your e-mail address,
# if you want to receive e-mail notifications on a non-default address
##BSUB -u your_email_address
### -- send notification at start --
#BSUB -B
### -- send notification at completion--
#BSUB -N
### -- Specify the output and error file. %J is the job-id --
### -- -o and -e mean append, -oo and -eo mean overwrite --
#BSUB -o out/%J.out
#BSUB -e err/%J.err
# -- end of LSF options --

# obtain job num
job_num=$LSB_JOBID

# activate anaconda without argument error
function DoSource(){
    source "/work3/s164180/miniconda3/bin/activate";
    conda activate thesis;
}
DoSource

## usefull keywords
# --ckpt_path
# --terminate_on_nan

# GPU stuff
#CUDA_VISIBLE_DEVICES=1
#ECHO $CUDA_VISIBLE_DEVICES
nvidia-smi

# command="python3 -m deepscm_jobs.train --train_batch_size 256 --validation batch_size 256 --model_name morphomnist --pgm_lr 1e-3 --lr 1e-4 --max_epochs 250 --data_dir ./generated_data/"


# $command --pgm conditional
# $command --pgm reverse
# $command --pgm full

# number of times to run
# count=10
# for i in $(seq $count); do
#     $command --pgm independent
# done


# data_dir = "assets/data/morphomnist"
# if "debug" in sys.argv or is_notebook():
#     args = ["--model_name", "digit", "--pgm", "full", "--data_dir", data_dir, "--train_batch_size", "256", "val_batch_size", "256", 
#             "--pgm_lr", "1e-3", "--num_workers", "8", "--max_epochs", "21", '--beta', "150"]

command="python3 -m jobs.train --model_name digit --pgm full --data_dir assets/data/morphomnist --train_batch_size 256 --val_batch_size 256 --pgm_lr 1e-3 --num_workers 8 --max_epochs 250" 

# $command --beta 1
# $command --beta 2
# $command --beta 5
# $command --beta 10
# $command --beta 25
# $command --beta 50
$command --beta 100
$command --beta 150
$command --beta 200
$command --beta 250
$command --beta 250