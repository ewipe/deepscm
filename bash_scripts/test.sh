#!/bin/sh
### General options
### –- specify queue --
#BSUB -q gpuv100
### -- set the job Name --
#BSUB -J deepscm
### -- ask for number of cores (default: 1) --
#BSUB -n 16
### -- Select the resources: 1 gpu in exclusive process mode --
#BSUB -gpu "num=1:mode=exclusive_process"
###BSUB -R "select[gpu24gb]", we uncomment this so we simple use the first available GPU
### -- set walltime limit: hh:mm --  maximum 24 hours for GPU-queues right now
#BSUB -W 24:00
# request 5GB of system-memory
#BSUB -R "rusage[mem=4GB]"
### -- set the email address --
# please uncomment the following line and put in your e-mail address,
# if you want to receive e-mail notifications on a non-default address
##BSUB -u your_email_address
### -- send notification at start --
#BSUB -B
### -- send notification at completion--
#BSUB -N
### -- Specify the output and error file. %J is the job-id --
### -- -o and -e mean append, -oo and -eo mean overwrite --
#BSUB -o out/%J.out
#BSUB -e err/%J.err
# -- end of LSF options --

# obtain job num
job_num=$LSB_JOBID

# activate anaconda without argument error
function DoSource(){
    source "/work3/s164180/miniconda3/bin/activate";
    conda activate thesis;
}
DoSource

python3 -m deepscm.test --test_batch_size 256 --ckpt_path morphomnist/generated_data_full/version_0/checkpoints/epoch=249-step=52750.ckpt --fast_dev_run 3
python3 -m deepscm.test --test_batch_size 256 --ckpt_path morphomnist/generated_data_independent/version_13/checkpoints/epoch=249-step=52750.ckpt --fast_dev_run 3
python3 -m deepscm.test --test_batch_size 256 --ckpt_path morphomnist/generated_data_conditional/version_5/checkpoints/epoch=249-step=52750.ckpt --fast_dev_run 3
python3 -m deepscm.test --test_batch_size 256 --ckpt_path morphomnist/generated_data_reverse/version_8/checkpoints/epoch=249-step=52750.ckpt --fast_dev_run 3