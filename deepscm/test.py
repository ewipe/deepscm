# %% imports

import os
import sys
from argparse import ArgumentParser

import torch

from pytorch_lightning import Trainer
from pytorch_lightning.loggers import TensorBoardLogger

from deepscm.util import load_model
from deepscm.util import is_notebook

import pyro

#### VERY IMPORTANT
pyro.clear_param_store()

# %%
##### ARGS
#ckpt_path = "morphomnist/generated_data_independent/version_13/checkpoints/epoch=249-step=52750.ckpt"
ckpt_path = "morphomnist/generated_data_conditional/version_5/checkpoints/epoch=249-step=52750.ckpt"
#ckpt_path = "morphomnist/generated_data_reverse/version_8/checkpoints/epoch=249-step=52750.ckpt"
#ckpt_path = "morphomnist/generated_data_full/version_0/checkpoints/epoch=249-step=52750.ckpt"

if "debug" in sys.argv or is_notebook():
    args = ["--test_batch_size", "256",
            "--ckpt_path", ckpt_path]
else:
    args = sys.argv

if "debug" in sys.argv:
    args.extend(["--fast_dev_run", "3"])

# add gpu if available
if torch.cuda.is_available():
    os.system("nvidia-smi")
    args.extend(["--accelerator", "gpu", "--devices", "1"])

print('args:\n',args)

# %% Train arguments
# Add program specific arguments
parser = ArgumentParser()

parser.add_argument("--ckpt_path", type=str, required=True, help="model checkpoint path)")
parser.add_argument('--test_batch_size', type=int, default = 4, help="Test batch size")
temp_args, _ = parser.parse_known_args(args=args)

# split ckpt path
path = os.path.normpath(temp_args.ckpt_path)
split_path = path.split(os.sep)

#take these from this
temp_args.default_root_dir = os.path.join(split_path[0],split_path[1],split_path[2])
temp_args.model_name = split_path[0]
temp_args.name = split_path[1]
temp_args.job_num = split_path[2]

# Pull the model name and class
LitModelClass = load_model(temp_args)
LitModel = LitModelClass.load_from_checkpoint(checkpoint_path=temp_args.ckpt_path, test_batch_size = temp_args.test_batch_size)
PyroModel = LitModel.pyro_model

with pyro.plate('observations', 256):
    pyro.render_model(PyroModel.model, model_args=(torch.ones(256,28,28),torch.ones(256,1), torch.ones(256,1)),render_distributions=True, render_params=False, filename='model.pdf')

# trainer args
train_parser = ArgumentParser()
train_parser = Trainer.add_argparse_args(train_parser)
train_args, _ = train_parser.parse_known_args(args=args)

if torch.cuda.is_available():
    trainer = Trainer.from_argparse_args(train_args, accelerator='gpu', devices=1)
else:
    trainer = Trainer.from_argparse_args(train_args)

logger = TensorBoardLogger(temp_args.model_name, temp_args.name, version=temp_args.job_num)
trainer.logger = logger

# %%% Do testing
trainer.test(LitModel, ckpt_path = temp_args.ckpt_path)

# %% printing
