
import os
from argparse import ArgumentParser

import torch
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger

from deepscm.load_model import load_model

if __name__ == '__main__':

    # Add program specific arguments
    parser = ArgumentParser()
    
    #parser.add_argument('--default_root_dir', type=str, required=True)
    parser.add_argument("--model_name", type=str, required=True, help="which model to choose: (default: %(default)s)")
    parser.add_argument("--job_num", type=int, default=0)
    parser.add_argument("--ckpt_path", type=str, default=None)

    # Pull the model name and class
    temp_args, _ = parser.parse_known_args()
    LitModelClass = load_model(temp_args)

    # Add model specific arguments
    model_parser = ArgumentParser()
    model_parser = LitModelClass.add_model_specific_args(model_parser)
    model_args, _ = model_parser.parse_known_args()
    LitModel = LitModelClass(**vars(model_args), model_name=temp_args.model_name)

    # trainer args
    train_parser = ArgumentParser()
    train_parser = Trainer.add_argparse_args(train_parser)
    train_args, _ = train_parser.parse_known_args()

    # Add more flexible logging
    name_ = os.path.basename(os.path.normpath(model_args.data_dir))
    pgm_ = model_args.pgm
    name = name_ + '_' + pgm_

    logger = TensorBoardLogger(temp_args.model_name,name,version=temp_args.job_num)
    #train_args.logger = logger

    # Add early stopping - don't add it. It doesn't add anything of what we want :)
    # early_stop_callback = EarlyStopping(monitor="val/loss", mode="min")
    trainer = Trainer.from_argparse_args(train_args)
    trainer.logger = logger # set logger this way...

    # fit
    trainer.fit(LitModel, ckpt_path = temp_args.ckpt_path)

    # test
    trainer.test(LitModel)