

# %%
import os
import sys
from argparse import ArgumentParser

import torch

from pytorch_lightning import Trainer
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from pytorch_lightning.loggers import TensorBoardLogger

from deepscm.util import load_model
from deepscm.util import is_notebook

import pyro

#### VERY IMPORTANT
pyro.clear_param_store()

# %%
##### ARGS
# "assets/data/morphomnist"
# "assets/data/intensity_causes_thickness"
# "generated_data"
#data_dir = "generated_data" 
data_dir = "assets/data/morphomnist"
if "debug" in sys.argv or is_notebook():
    args = ["--model_name", "morphomnist", "--pgm", "full", "--data_dir", data_dir, "--train_batch_size", "256", "val_batch_size", "256", 
            "--pgm_lr", "1e-3", "--num_workers", "8", "--max_epochs", "21", '--beta', "1"]
else:
    args = sys.argv

# add gpu if available
if torch.cuda.is_available():
    os.system("nvidia-smi")
    args.extend(["--accelerator", "gpu", "--devices", "1"])

print('args:\n',args)

# %% define class
parser = ArgumentParser()

#parser.add_argument('--default_root_dir', type=str, required=True)
parser.add_argument("--model_name", type=str, required=True, help="which model to choose: (default: %(default)s)")
parser.add_argument("--job_num", type=int, default=None)
#parser.add_argument("--ckpt_path", type=str, default=None)

# Pull the model name and class
temp_args, _ = parser.parse_known_args(args=args)
LitModelClass = load_model(temp_args)

# Add model specific arguments
model_parser = ArgumentParser()
model_parser = LitModelClass.add_model_specific_args(model_parser)
model_args, _ = model_parser.parse_known_args(args=args)
LitModel = LitModelClass(**vars(model_args), model_name=temp_args.model_name)
pyro_model = LitModel.pyro_model


# %%
# trainer args
train_parser = ArgumentParser()
train_parser = Trainer.add_argparse_args(train_parser)
train_args, _ = train_parser.parse_known_args(args=args)

# Add more flexible logging
name_ = os.path.basename(os.path.normpath(model_args.data_dir))
pgm_ = model_args.pgm
name = name_ + '_' + pgm_

logger = TensorBoardLogger(temp_args.model_name,name,version=temp_args.job_num)
train_args.logger = logger

trainer = Trainer.from_argparse_args(train_args)
trainer.logger = logger # set logger this way...

# with pyro.plate('observations', 256):
#     a = pyro.render_model(pyro_model.model, model_args=(torch.ones(256,28,28),torch.ones(256,1), torch.ones(256,1)),render_distributions=True, render_params=False)
# a
# %%
# fit trainer
trainer.fit(LitModel)
#end_params = pyro_model.state_dict().copy()

# %% run plots for notebook
if is_notebook():
    import pandas as pd
    thickness, intensity = LitModel.mnist_test.metrics.values()
    d = {'thickness': thickness, 'intensity': intensity}
    test = pd.DataFrame(d)
    # %% render model
    # pyro.render_model(pyro_model.model, model_args=(torch.empty(256,28,28),torch.empty(256,1), torch.empty(256,1)),render_distributions=True, render_params=False)

    # %%
    ### Quick sampling
    def quick_fun(x):
        try:
            foo = x.cpu().numpy().squeeze()
            return foo 
        except:
            return x

    with torch.no_grad():
        _ = pyro_model.sample(10000)
    x, z, thickness_, intensity_ = [quick_fun(x) for x in _]
    samples = pd.DataFrame({'thickness': thickness_, 'intensity': intensity_})

    # %%
    # display thickness
    import matplotlib.pyplot as plt
    plt.figure()
    plt.hist(thickness_, bins=50, alpha=0.3, density=True, label='Samples')
    plt.hist(thickness, bins=50, alpha=0.3, density=True, label = 'Test data')
    plt.legend()
    plt.show()

    # %%
    # display intensity
    import matplotlib.pyplot as plt
    plt.figure()
    plt.hist(intensity_, bins=50, alpha=0.3, density=True, label='Samples')
    plt.hist(intensity, bins=50, alpha=0.3, density=True, label = 'Test data')
    plt.legend()
    plt.show()
    
    
    
    # %%
    from matplotlib.colors import ListedColormap
    import seaborn as sns
    #sns.set_theme()
    from itertools import cycle
    col_pal = cycle(sns.color_palette("rocket"))
    
    # make figures like this, we actually hate KDE's!
    axs = sns.jointplot(x='thickness', y='intensity', data=test, label = 'Test Data', alpha=0.3, zorder=10, color=next(col_pal), xlim=(1/5,10))    
    axs = sns.jointplot(x='thickness', y='intensity', data=samples, label = 'Flow Samples', alpha=0.3, zorder=10, color=next(col_pal), xlim=(1/5,10))


    # %% 2d histograms - we like this a lot!
    sns.set_theme()
    h = sns.jointplot(data=test, x="thickness", y="intensity", marginal_ticks=True, kind = 'hist', joint_kws ={'bins':50, "cbar": True, "stat": "count"},  xlim=(0,10))
    h.set_axis_labels('Thickness', 'Intensity', fontsize=16)
    h.savefig('intensity_causes_thickness_test_samples.pdf')
    # %% 2d histogram of generates samples
    h = sns.jointplot(data=samples, x="thickness", y="intensity", marginal_ticks=True, kind = 'hist', joint_kws ={'bins':50, "cbar": True, "stat": "count"},  xlim=(0,10))
    h.set_axis_labels('Thickness', 'Intensity', fontsize=16)
# %%



# %%
