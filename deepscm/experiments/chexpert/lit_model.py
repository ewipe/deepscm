
import os
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader, Dataset
import pandas as pd
import numpy as np
import torchvision
import torchvision.transforms as T
from torchvision import models
import pytorch_lightning as pl

from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import ModelCheckpoint
from skimage.io import imread
from skimage.io import imsave
from tqdm import tqdm
from argparse import ArgumentParser

image_size = (224, 224)


class CheXpertDataset(Dataset):
    def __init__(self, csv_file_img, image_size, augmentation=False, pseudo_rgb = True, img_data_dir = '/work3/s164180/chexploration/datafiles/CheXpert-v1.0-small'):
        self.data = pd.read_csv(csv_file_img)
        self.image_size = image_size
        self.do_augment = augmentation
        self.pseudo_rgb = pseudo_rgb

        self.augment = T.Compose([
            T.RandomHorizontalFlip(p=0.5),
            T.RandomApply(transforms=[T.RandomAffine(degrees=15, scale=(0.9, 1.1))], p=0.5),
        ])

        self.samples = []
        for idx, _ in enumerate(tqdm(range(len(self.data)), desc='Loading Data')):
            img_path = img_data_dir + self.data.loc[idx, 'path_preproc']
            img_label = np.array(self.data.loc[idx, 'sex_label'], dtype='int64')

            sample = {'image_path': img_path, 'label': img_label}
            self.samples.append(sample)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, item):
        sample = self.get_sample(item)

        image = torch.from_numpy(sample['image']).unsqueeze(0)
        label = torch.from_numpy(sample['label'])

        if self.do_augment:
            image = self.augment(image)

        if self.pseudo_rgb:
            image = image.repeat(3, 1, 1)

        return {'image': image, 'label': label}

    def get_sample(self, item):
        sample = self.samples[item]
        image = imread(sample['image_path']).astype(np.float32)

        return {'image': image, 'label': sample['label']}


class CheXpertExperiment(pl.LightningModule):
    def __init__(self, **kwargs):
        super().__init__()

        # Initiate kwarg key value pairs as methods
        for key, value in kwargs.items():
            setattr(self, key, value)

        # save hyper parameters
        self.save_hyperparameters()


    def prepare_data(self):
        # prepare transforms standard to Chexploration

        self.train_set = CheXpertDataset(self.csv_train_img, self.image_size, augmentation=True)
        self.val_set = CheXpertDataset(self.csv_val_img, self.image_size, augmentation=False)
        self.test_set = CheXpertDataset(self.csv_test_img, self.image_size, augmentation=False)

        print('#train: ', len(self.train_set))
        print('#val:   ', len(self.val_set))
        print('#test:  ', len(self.test_set))

    def train_dataloader(self):
        return DataLoader(self.train_set, self.batch_size, shuffle=True, num_workers=self.num_workers)

    def val_dataloader(self):
        return DataLoader(self.val_set, self.batch_size, shuffle=False, num_workers=self.num_workers)

    def test_dataloader(self):
        return DataLoader(self.test_set, self.batch_size, shuffle=False, num_workers=self.num_workers)

        raise NotImplementedError

    def configure_optimizers(self):
        # We use Pyros internal optimizers
        raise NotImplementedError

    def forward(self, *args, **kwargs):
        pass

    def get_batch(self, loader):
        batch = next(iter(self.val_loader))
        batch = self.trainer.strategy.batch_to_device(batch)
        return batch

    def build_reconstruction(self, **kwargs):
        raise NotImplementedError

    def backward(self, *args, **kwargs):
        raise NotImplementedError

    def prep_batch(self, batch):
        raise NotImplementedError

    def training_step(self, batch, batch_idx):
        raise NotImplementedError

    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = parent_parser.add_argument_group("LitModel")
        
        # hparams related to data
        parser.add_argument('--train_batch_size', default=256, type=int, help="train batch size (default: %(default)s)")
        parser.add_argument('--test_batch_size', default=256, type=int, help="test batch size (default: %(default)s)")
        parser.add_argument('--val_batch_size', default=256, type=int, help="validation batch size (default: %(default)s)")
        parser.add_argument('--num_workers', type=int, default=eval(f"{os.cpu_count()}"))
        parser.add_argument('--data_dir', default="assets/data/morphomnist", type=str, help="data dir (default: %(default)s)")

        #hparams related to training
        parser.add_argument('--sample_img_interval', default=10, type=int, help="interval in which to sample and log images (default: %(default)s)")


        return parent_parser