from multiprocessing.sharedctypes import Value
import pyro

from pyro.nn import PyroModule, pyro_method

from pyro.distributions import TransformedDistribution
from pyro.infer.reparam.transform import TransformReparam
from torch.distributions import Independent

from deepscm.datasets.morphomnist import MorphoMNISTLike
from pyro.distributions.transforms import ComposeTransform, SigmoidTransform, AffineTransform

import torchvision.utils
from torch.utils.data import DataLoader, random_split
import pytorch_lightning as pl
import torch
import numpy as np

import seaborn as sns
import matplotlib.pyplot as plt

from deepscm.morphomnist import measure
import os
from functools import partial
import multiprocessing

from deepscm.distributions.deep import DeepMultivariateNormal, DeepIndepNormal, Conv2dIndepNormal, DeepLowRankMultivariateNormal
from pyro.infer import SVI, TraceGraph_ELBO
from pyro.optim import Adam, ClippedAdam
import pandas as pd

class CustomELBO(TraceGraph_ELBO):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.trace_storage = {'model': None, 'guide': None}

    def _get_trace(self, model, guide, args, kwargs):
        model_trace, guide_trace = super()._get_trace(model, guide, args, kwargs)

        self.trace_storage['model'] = model_trace
        self.trace_storage['guide'] = guide_trace

        return model_trace, guide_trace

class Experiment(pl.LightningModule):
    def __init__(self, **kwargs):
        super().__init__()
        
        # Initiate kwarg key value pairs as methods
        for key, value in kwargs.items():
            setattr(self, key, value)

        # save hyper parameters
        self.save_hyperparameters()
        
        
        if self.validate:
            import random
            torch.manual_seed(0)
            np.random.seed(0)
            random.seed(0)

            torch.backends.cudnn.deterministic = True
            torch.backends.cudnn.benchmark = False
            torch.autograd.set_detect_anomaly(self.hparams.validate)
            pyro.enable_validation()
        
        # indicate that we are using our own optimizer
        #self.automatic_optimization=False

        # Initiate pyro model parameters
        if self.pgm == 'independent':
            from .independent import IndependentVISEM
            pyro_model = IndependentVISEM
        elif self.pgm == 'conditional':
            from .conditional import ConditionalVISEM
            pyro_model = ConditionalVISEM
        elif self.pgm == 'full':
            from .full import FullVISEM
            pyro_model = FullVISEM
        elif self.pgm == 'reverse':
            from .reverse import ReverseVISEM
            pyro_model = ReverseVISEM
        elif self.pgm == "test":
            from .test import TestVISEM
            pyro_model = TestVISEM
        else:
            raise ValueError(f"Please specify a proper pgm, got {self.pgm}")
        
        self.pyro_model = pyro_model(**kwargs)

        ## add beta for beta VAE
        self.pyro_model.beta = self.hparams.beta
        
        # init svi loss
        self.svi_loss = CustomELBO(num_particles=self.hparams.num_svi_particles)
        self._build_svi()

        # counterfactuals
        self.pyro_model._gen_counterfactual = partial(self.pyro_model.counterfactual, num_particles=self.num_sample_particles)

    
    def _build_svi(self, loss=None):
            def per_param_callable(module_name, param_name):
                params = {'eps': 1e-5, 'amsgrad': self.hparams.use_amsgrad, 'weight_decay': self.hparams.l2}
                if module_name == 'intensity_flow_components' or module_name == 'thickness_flow_components':
                    params['lr'] = self.hparams.pgm_lr
                    return params
                else:
                    params['lr'] = self.hparams.lr
                    return params

            if loss is None:
                loss = self.svi_loss
            self.svi = SVI(self.pyro_model.svi_model, self.pyro_model.svi_guide, Adam(per_param_callable), loss)
            self.svi.loss_class = loss

    def prepare_data(self):
        # prepare transforms standard to MNIST
        mnist_train = MorphoMNISTLike(self.data_dir, train=True, columns=['thickness', 'intensity'])
        self.mnist_test = MorphoMNISTLike(self.data_dir, train=False, columns=['thickness', 'intensity'])

        num_val = int(len(mnist_train) * 0.1)
        num_train = len(mnist_train) - num_val
        self.mnist_train, self.mnist_val = random_split(mnist_train, [num_train, num_val])

        ## Set device and range 
        self.torch_device = self.trainer.strategy.root_device
        
        print(f'using device: {self.torch_device}')
        
        # stuff below should be moved
        thicknesses = 1. + torch.arange(3, dtype=torch.float, device=self.torch_device)
        self.thickness_range = thicknesses.repeat(3).unsqueeze(1)
        intensity = 48 * torch.arange(3, dtype=torch.float, device=self.torch_device) + 64
        self.intensity_range = intensity.repeat_interleave(3).unsqueeze(1)
        self.z_range = torch.randn([1, self.hparams.latent_dim], device=self.torch_device, dtype=torch.float).repeat((9, 1))

        self.pyro_model.intensity_flow_norm_loc = mnist_train.metrics['intensity'].min().to(self.torch_device).float()
        self.pyro_model.intensity_flow_norm_scale = (mnist_train.metrics['intensity'].max() - mnist_train.metrics['intensity'].min()).to(self.torch_device).float()  # noqa: E501
        
        print(f'set intensity_flow_norm.loc to {self.pyro_model.intensity_flow_norm_loc}')
        print(f'set intensity_flow_norm.scale to {self.pyro_model.intensity_flow_norm_scale}')

        self.pyro_model.thickness_flow_lognorm_loc = mnist_train.metrics['thickness'].log().mean().to(self.torch_device).float()
        self.pyro_model.thickness_flow_lognorm_scale = mnist_train.metrics['thickness'].log().std().to(self.torch_device).float()

        print(f'set thickness_flow_lognorm.loc to {self.pyro_model.thickness_flow_lognorm_loc}')
        print(f'set thickness_flow_lognorm.scale to {self.pyro_model.thickness_flow_lognorm_scale}')

    def configure_optimizers(self):
        # We use Pyros internal optimizers
        pass

    def train_dataloader(self):
        return DataLoader(self.mnist_train, batch_size=self.train_batch_size, shuffle=True, num_workers=self.num_workers)

    def val_dataloader(self):
        self.val_loader = DataLoader(self.mnist_val, batch_size=self.val_batch_size, shuffle=False, num_workers = self.num_workers)
        return self.val_loader

    def test_dataloader(self):
        self.test_loader = DataLoader(self.mnist_test, batch_size=self.test_batch_size, shuffle=False, num_workers = self.num_workers)
        return self.test_loader

    def forward(self, *args, **kwargs):
        pass

    def validation_epoch_end(self, outputs):
        outputs = self.assemble_epoch_end_outputs(outputs)
        metrics = {('val/' + k): v for k, v in outputs.items()}
        self.log_dict(metrics)

    def assemble_epoch_end_outputs(self, outputs):
        num_items = len(outputs)

        def handle_row(batch, assembled=None):
            if assembled is None:
                assembled = {}

            # for k, v in batch.items():
            #     if k not in assembled.keys():
            #         if isinstance(v, dict):
            #             assembled[k] = handle_row(v)
            #         elif isinstance(v, float):
            #             assembled[k] = v
            #         elif np.prod(v.shape) == 1:
            #             assembled[k] = v.cpu()
            #         else:
            #             assembled[k] = v.cpu()
            #     else:
            #         if isinstance(v, dict):
            #             assembled[k] = handle_row(v, assembled[k])
            #         elif isinstance(v, float):
            #             assembled[k] += v
            #         elif np.prod(v.shape) == 1:
            #             assembled[k] += v.cpu()
            #         else:
            #             assembled[k] = torch.cat([assembled[k], v.cpu()], 0)

            for k, v in batch.items():
                if k not in assembled.keys():
                    if isinstance(v, dict):
                        assembled[k] = handle_row(v)
                    elif isinstance(v, float):
                        assembled[k] = v
                    elif np.prod(v.shape) == 1:
                        assembled[k] = v
                    else:
                        assembled[k] = v
                else:
                    if isinstance(v, dict):
                        assembled[k] = handle_row(v, assembled[k])
                    elif isinstance(v, float):
                        assembled[k] += v
                    elif np.prod(v.shape) == 1:
                        assembled[k] += v
                    else:
                        assembled[k] = torch.cat([assembled[k], v], 0)

            return assembled

        assembled = {}
        for _, batch in enumerate(outputs):
            assembled = handle_row(batch, assembled)

        for k, v in assembled.items():
            if (hasattr(v, 'shape') and np.prod(v.shape) == 1) or isinstance(v, float):
                assembled[k] /= num_items

        return assembled

    def get_batch(self, loader):
        batch = next(iter(self.val_loader))
        batch = self.trainer.strategy.batch_to_device(batch)
        return batch

    def backward(self, *args, **kwargs):
        pass  # No loss to backpropagate since we're using Pyro's optimisation machinery

    def get_trace_metrics(self, batch):
        metrics = {}
        
        model = self.svi.loss_class.trace_storage['model']
        guide = self.svi.loss_class.trace_storage['guide']
        

        metrics['log p(intensity)'] = model.nodes['intensity']['log_prob'].mean()
        metrics['log p(thickness)'] = model.nodes['thickness']['log_prob'].mean()
        metrics['log p(x)'] = model.nodes['x']['log_prob'].mean()
        metrics['p(z)'] = model.nodes['z']['log_prob'].mean()
        metrics['q(z)'] = guide.nodes['z']['log_prob'].mean()
        metrics['log p(z) - log q(z)'] = metrics['p(z)'] - metrics['q(z)']
         
        return metrics

    def prep_batch(self, batch):
        x = batch['image']
        thickness = batch['thickness'].unsqueeze(1).float()
        intensity = batch['intensity'].unsqueeze(1).float()

        x = x.float()

        if self.training:
            x += torch.rand_like(x)

        x = x.unsqueeze(1)

        return {'x': x, 'thickness': thickness, 'intensity': intensity}
    
    def training_step(self, batch, batch_idx):
        
        batch = self.prep_batch(batch)
        loss = self.svi.step(**batch)

        metrics = self.get_trace_metrics(batch)

        if np.isnan(loss):
            self.logger.experiment.add_text('nan', f'nand at {self.current_epoch}:\n{metrics}')
            raise ValueError('loss went to nan with metrics:\n{}'.format(metrics))

        tensorboard_logs = {('train/' + k): v for k, v in metrics.items()}
        tensorboard_logs['train/loss'] = loss

        self.log_dict(tensorboard_logs)

        return torch.Tensor([loss])

    def validation_step(self, batch, batch_idx):
        batch = self.prep_batch(batch)

        loss = self.svi.evaluate_loss(**batch)

        metrics = self.get_trace_metrics(batch)

        return {'loss': loss, **metrics}
    
    def test_step(self, batch, batch_idx):

        batch = self.prep_batch(batch)
        loss = self.svi.evaluate_loss(**batch)
        metrics = self.get_trace_metrics(batch)

        return {'loss': loss, 'test_batches': batch,
                **metrics}
    
    
    def validation_epoch_end(self, outputs):
        outputs = self.assemble_epoch_end_outputs(outputs)

        metrics = {('val/' + k): v for k, v in outputs.items()}

        self.log_dict(metrics)
    
    def test_epoch_end(self, outputs):
        print('assembling outputs')
        outputs = self.assemble_epoch_end_outputs(outputs)

        # save metrics
        # print('Saving Metrics')
        # keys = ['loss', 'log p(intensity)', 'log p(thickness)', 'log p(x)', 'p(z)', 'q(z)', 'log p(z) - log q(z)']
        # metrics = {('test/' + k): outputs[k] for k in keys}
        # p = os.path.join(self.trainer.logger.experiment.log_dir, 'metrics.pt')
        # torch.save(metrics, p)        

        # # make 8 reconstructions
        # print('Making Reconstructions')
        # obs = outputs['test_batches']
        # self.make_reconstructions(obs)
        
        # # save 10000 thickness and intensity samples
        # print('Sampling unconditional samples')
        # with torch.no_grad():
        #     x,z,thickness,intensity = self.pyro_model.sample(10000)
        #     measured_thickness, measured_intensity = self.measure_image(x)
        
        # samples = pd.DataFrame({'thickness': thickness.cpu().squeeze().numpy(), 'intensity': intensity.cpu().squeeze().numpy(), 
        #                         'measured_thickness': measured_thickness, 'measured_intensity': measured_intensity})
        # p = os.path.join(self.trainer.logger.experiment.log_dir, 'covariate_samples.pt')
        # torch.save(samples, f=p)

        # print('Unconditional samples')
        # # save 16 samples drawn
        # x_sample = x[:16]
        # p = os.path.join(self.trainer.logger.experiment.log_dir, 'image_samples.pt')
        # self.log_img_grid('test/samples', x_sample, nrow=4, normalize=True)
        # torch.save(x_sample, f =p)

        # # conditional samples
        # print('Conditional samples')
        
        # intensity_range = torch.tensor([64, 128, 192, 255], device=self.torch_device, dtype=torch.float).unsqueeze(1)
        # thickness_range = torch.tensor([1, 2, 4, 6], device=self.torch_device, dtype=torch.float).unsqueeze(1)
        # samples = []
        # z = torch.randn([1, self.hparams.latent_dim], device=self.torch_device, dtype=torch.float)
        # for i, thickness in enumerate(thickness_range):
        #     for j, intensity in enumerate(intensity_range):
        #         data = {'z': z, 'thickness': thickness.unsqueeze(1), 'intensity': intensity.unsqueeze(1)}
        #         sample, *_ = pyro.condition(self.pyro_model.sample, data=data)(1)

        #         samples += [sample.squeeze(0)]

        # self.log_img_grid('test/cond_samples', samples, nrow=4)

        # p = os.path.join(self.trainer.logger.experiment.log_dir, 'cond_sampels.pt')
        # torch.save(samples, f=p)

        # counterfactuals
        # print('Make counterfactuals')

        # interventions = [
        # {'thickness': 1.},
        # {'thickness': 4.},
        # {'thickness': 7.},
        # {'intensity': 64.},
        # {'intensity': 160.},
        # {'intensity': 255.}
        # ]


        # # get batch
        # counterfactual_dict = {}
        # counterfactual_dict['thickness'] = []
        # counterfactual_dict['intensity'] = []
        # obs = outputs['test_batches']
        # imgs = []
        # test_num = [1337, 13, 42]

        # for num in test_num:
            
        #     x = obs['x'][num].reshape((1,1,28,28))
        #     thickness = obs['thickness'][num].reshape((1,1))
        #     intensity = obs['intensity'][num].reshape((1,1))

        #     counterfactual_dict['thickness'].append(thickness)
        #     counterfactual_dict['intensity'].append(intensity)

        #     orig_data = {'x':x, 'thickness': thickness, 'intensity': intensity}

        #     imgs.append(x.reshape((1,28,28)))

        #     for intervention in interventions:
        #         pyro.clear_param_store()
        #         cond = {k: torch.tensor([[v]], device=self.torch_device) for k, v in intervention.items()}
        #         counterfactual = self.pyro_model.counterfactual(orig_data, cond, self.num_sample_particles)

        #         imgs.append(counterfactual['x'].reshape((1,28,28)))
        
        # self.log_img_grid('test/counterfactuals', imgs, nrow=len(interventions) + 1)
        
        # counterfactual_dict['imgs'] = imgs
        # counterfactual_dict['intervensions'] = interventions

        # p = os.path.join(self.trainer.logger.experiment.log_dir, 'counterfactuals.pt')
        # torch.save(counterfactual_dict, p)

        # make kde prob maps
        # print('Making prob maps')
        # self.make_prob_maps()

        # intervention vs condition for reverse and full
        # if self.pgm == "reverse" or self.pgm == "full" or self.pgm == 'conditional':
        #     print('Making ind vs cond for reverse pgm')
        #     self.make_int_cond()

        print('Making prob maps over intensity')
        self.make_prob_maps_2()

        print('Testing Finished')

    def make_prob_maps_2(self):

        self.pyro_model.eval()
        model = self.pyro_model.pgm_model
        prob_maps = {}

        intensity_range = torch.arange(64, 255, 0.5, dtype=torch.float, device=self.trainer.strategy.root_device)
        thickness_range = torch.arange(.5, 9, 0.01, dtype=torch.float, device=self.trainer.strategy.root_device)


        num_intensity = intensity_range.shape[0]
        num_thickness = thickness_range.shape[0]

        intensity_range = intensity_range.repeat(num_thickness).unsqueeze(1)
        thickness_range = thickness_range.repeat_interleave(num_intensity).unsqueeze(1)

        cond_data = {
            'thickness': thickness_range,
            'intensity': intensity_range
        }
        with torch.no_grad():
            trace = pyro.poutine.trace(pyro.condition(model, data=cond_data)).get_trace(num_intensity*num_thickness)
            trace.compute_log_prob()

        log_prob = trace.nodes['thickness']['log_prob'] + trace.nodes['intensity']['log_prob']

        prob_maps['$p(t, i)$'] = {
        'log_prob': log_prob, 'intensity': trace.nodes['_RETURN']['value'][1], 'thickness': trace.nodes['_RETURN']['value'][0]
        }

        intervention_data = {'intensity': np.clip(cond_data['intensity'] + 32,0,300)} # add these to avoid nan

        with torch.no_grad():
            trace = pyro.poutine.trace(pyro.condition(pyro.do(model, data=intervention_data), data=cond_data)).get_trace(num_intensity*num_thickness)
            trace.compute_log_prob()
        log_prob = trace.nodes['thickness']['log_prob'] + trace.nodes['intensity']['log_prob']
        prob_maps['$p(t, i\,|\,do(i + 32))$'] = {
        'log_prob': log_prob, 'intensity': trace.nodes['_RETURN']['value'][1], 'thickness': trace.nodes['_RETURN']['value'][0]
        }

        intervention_data = {'intensity': np.clip(cond_data['intensity'] - 32, 0, 300)} # add these to avoid nan

        with torch.no_grad():
            trace = pyro.poutine.trace(pyro.condition(pyro.do(model, data=intervention_data), data=cond_data)).get_trace(num_intensity*num_thickness)
            trace.compute_log_prob()
        log_prob = trace.nodes['thickness']['log_prob'] + trace.nodes['intensity']['log_prob']
        prob_maps['$p(t, i\,|\,do(i - 32))$'] = {
        'log_prob': log_prob, 'intensity': trace.nodes['_RETURN']['value'][1], 'thickness': trace.nodes['_RETURN']['value'][0]
        }

        p = os.path.join(self.trainer.logger.experiment.log_dir, 'prob_maps_int.pt')
        torch.save(prob_maps, p)

    
    def make_int_cond(self):
        self.pyro_model.eval()
        model = self.pyro_model.pgm_model
        intensity_range = torch.arange(64, 255, 0.5, dtype=torch.float, device=self.trainer.strategy.root_device)
        thickness_range = torch.arange(.5, 9, 0.01, dtype=torch.float, device=self.trainer.strategy.root_device)
        num_intensity = intensity_range.shape[0]
        num_thickness = thickness_range.shape[0]

        intensity_range = intensity_range.repeat(num_thickness).unsqueeze(1)
        thickness_range = thickness_range.repeat_interleave(num_intensity).unsqueeze(1)

        thickness = thickness_range.reshape(num_thickness, num_intensity)
        intensity = intensity_range.reshape(num_thickness, num_intensity)

        interv_i = [96, 160, 224]
        interv_t = [1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5]

        make_int_cond_dict = {}

        make_int_cond_dict['cond_i'] = {}
        make_int_cond_dict['do_i'] = {}
        make_int_cond_dict['cond_t'] = {}
        make_int_cond_dict['do_t'] = {}

        if self.pgm == 'reverse':
            # First condition
            for do_i in interv_i:
                make_int_cond_dict['cond_i'][do_i] ={}
                with torch.no_grad():
                    trace = pyro.poutine.trace(pyro.condition(model, data = {
                        'thickness': thickness[:,0].unsqueeze(1),
                        'intensity': torch.full_like(thickness[:, 0], do_i).unsqueeze(1)
                    })).get_trace(num_thickness)
                    trace.compute_log_prob()
                
                # Bayes' rule: p(t|i) \propto p(t) * p(i|t)
                prob = (trace.nodes['thickness']['log_prob'] + trace.nodes['intensity']['log_prob']).exp()
                make_int_cond_dict['cond_i'][do_i]['prob'] = prob
                make_int_cond_dict['cond_i'][do_i]['thickness'] = thickness

            # Second intervention
            for do_i in interv_i:
                make_int_cond_dict['do_i'][do_i] = {}
                with torch.no_grad():
                    trace = pyro.poutine.trace(pyro.condition(
                        pyro.do(model, data={
                            'intensity': torch.full_like(thickness[:, 0], do_i).unsqueeze(1)
                        }), data={
                        'thickness': thickness[:, 0].unsqueeze(1)
                    })).get_trace(num_thickness)
                    trace.compute_log_prob()
                
                prob = (trace.nodes['thickness']['log_prob']).exp()
                make_int_cond_dict['do_i'][do_i]['prob'] = prob
                make_int_cond_dict['do_i'][do_i]['thickness'] = thickness
            
            # Condition on thickness
            for do_t in interv_t:
                make_int_cond_dict['cond_t'][do_t] = {}
                with torch.no_grad():
                    trace = pyro.poutine.trace(pyro.condition(
                        pyro.do(model, data={
                            'thickness': torch.full_like(intensity[0, :], do_t).unsqueeze(1),
                        }), data={
                        'intensity': intensity[0, :].unsqueeze(1)
                    })).get_trace(num_intensity)
                    trace.compute_log_prob()
                
                prob = trace.nodes['intensity']['log_prob'].exp()
                make_int_cond_dict['cond_t'][do_t]['prob'] = prob
                make_int_cond_dict['cond_t'][do_t]['intensity'] = intensity
        else:
            #  intervene on i
            for do_i in interv_i:
                make_int_cond_dict['do_i'][do_i] = {}
                with torch.no_grad():
                    trace = pyro.poutine.trace(pyro.condition(
                        pyro.do(model, data={
                            'intensity': torch.full_like(thickness[:, 0], do_i).unsqueeze(1)
                        }), data={
                        'thickness': thickness[:, 0].unsqueeze(1)
                    })).get_trace(num_thickness)
                    trace.compute_log_prob()
                
                prob = (trace.nodes['thickness']['log_prob']).exp()
                make_int_cond_dict['do_i'][do_i]['prob'] = prob
                make_int_cond_dict['do_i'][do_i]['thickness'] = thickness
            
            # condition on t
            for do_t in interv_t:
                make_int_cond_dict['cond_t'][do_t] ={}
                with torch.no_grad():
                    trace = pyro.poutine.trace(pyro.condition(model, data = {
                        'intensity': intensity[0,:].unsqueeze(1),
                        'thickness': torch.full_like(intensity[0, :], do_t).unsqueeze(1)
                    })).get_trace(num_intensity)
                    trace.compute_log_prob()
                
                # Bayes' rule: p(t|i) \propto p(t) * p(i|t)
                prob = (trace.nodes['thickness']['log_prob'] + trace.nodes['intensity']['log_prob']).exp()
                make_int_cond_dict['cond_t'][do_t]['prob'] = prob
                make_int_cond_dict['cond_t'][do_t]['intensity'] = intensity
                make_int_cond_dict['cond_t'][do_t]['prob_2'] =  (trace.nodes['intensity']['log_prob']).exp()
            
            # intervene on t
            pyro.clear_param_store()
            for do_t in interv_t:
                make_int_cond_dict['do_t'][do_t] = {}
                with torch.no_grad():
                    trace = pyro.poutine.trace(pyro.condition(
                        pyro.do(model, data={
                            'thickness': torch.full_like(intensity[0, :], do_t).unsqueeze(1)
                        }), data={
                        'intensity': intensity[0, :].unsqueeze(1)
                    })).get_trace(num_intensity)
                    trace.compute_log_prob()
                
                prob = (trace.nodes['intensity']['log_prob']).exp()
                make_int_cond_dict['do_t'][do_t]['prob'] = prob
                make_int_cond_dict['do_t'][do_t]['intensity'] = intensity
            



        
        p = os.path.join(self.trainer.logger.experiment.log_dir, 'int_cond_dict.pt')
        torch.save(make_int_cond_dict, p)

    def make_prob_maps(self):

        self.pyro_model.eval()
        model = self.pyro_model.pgm_model
        prob_maps = {}

        intensity_range = torch.arange(64, 255, 0.5, dtype=torch.float, device=self.trainer.strategy.root_device)
        thickness_range = torch.arange(.5, 9, 0.01, dtype=torch.float, device=self.trainer.strategy.root_device)


        num_intensity = intensity_range.shape[0]
        num_thickness = thickness_range.shape[0]

        intensity_range = intensity_range.repeat(num_thickness).unsqueeze(1)
        thickness_range = thickness_range.repeat_interleave(num_intensity).unsqueeze(1)

        cond_data = {
            'thickness': thickness_range,
            'intensity': intensity_range
        }
        with torch.no_grad():
            trace = pyro.poutine.trace(pyro.condition(model, data=cond_data)).get_trace(num_intensity*num_thickness)
            trace.compute_log_prob()

        log_prob = trace.nodes['thickness']['log_prob'] + trace.nodes['intensity']['log_prob']
        # orig_thickness_fn = trace.nodes['thickness']['fn']
        # orig_intensity_fn = trace.nodes['intensity']['fn']

        prob_maps['$p(t, i)$'] = {
        'log_prob': log_prob, 'intensity': trace.nodes['_RETURN']['value'][1], 'thickness': trace.nodes['_RETURN']['value'][0]
        }

        intervention_data = {'thickness': cond_data['thickness'] + 1}
        with torch.no_grad():
            trace = pyro.poutine.trace(pyro.condition(pyro.do(model, data=intervention_data), data=cond_data)).get_trace(num_intensity*num_thickness)
            trace.compute_log_prob()
        log_prob = trace.nodes['thickness']['log_prob'] + trace.nodes['intensity']['log_prob']
        prob_maps['$p(t, i\,|\,do(t + 1))$'] = {
        'log_prob': log_prob, 'intensity': trace.nodes['_RETURN']['value'][1], 'thickness': trace.nodes['_RETURN']['value'][0]
        }

        intervention_data = {'thickness': np.clip(cond_data['thickness'] - 0.5, 0.001, 10)} # add these to avoid nan

        with torch.no_grad():
            trace = pyro.poutine.trace(pyro.condition(pyro.do(model, data=intervention_data), data=cond_data)).get_trace(num_intensity*num_thickness)
            trace.compute_log_prob()
        
        log_prob = trace.nodes['thickness']['log_prob'] + trace.nodes['intensity']['log_prob']
        prob_maps['$p(t, i\,|\,do(t - 0.5))$'] = {
        'log_prob': log_prob, 'intensity': trace.nodes['_RETURN']['value'][1], 'thickness': trace.nodes['_RETURN']['value'][0]
        }
        p = os.path.join(self.trainer.logger.experiment.log_dir, 'prob_maps.pt')
        torch.save(prob_maps, p)


    def measure_image(self, x, normalize=False, threshold=0.5):
        imgs = x.detach().cpu().numpy()[:, 0]

        if normalize:
            imgs -= imgs.min()
            imgs /= imgs.max() + 1e-6

        with multiprocessing.Pool() as pool:
            measurements = measure.measure_batch(imgs, threshold=threshold, pool=pool)

        def get_intensity(imgs, threshold):

            img_min, img_max = imgs.min(axis=(1, 2), keepdims=True), imgs.max(axis=(1, 2), keepdims=True)
            mask = (imgs >= img_min + (img_max - img_min) * threshold)

            return np.array([np.median(i[m]) for i, m in zip(imgs, mask)])

        return measurements['thickness'].values, get_intensity(imgs, threshold)

    def make_reconstructions(self, obs, n=8,**kwargs):
        x = obs['x'][:n,]
        thickness = obs['thickness'][:n,]
        intensity = obs['intensity'][:n,]
        recon = self.pyro_model.reconstruct(x = x, thickness = thickness, intensity = intensity, num_particles=self.num_sample_particles)
        self.log_img_grid('test/reconstructions', torch.cat([x, recon], 0), nrow=n, **kwargs)

        p = os.path.join(self.trainer.logger.experiment.log_dir, 'reconstructions.pt')

        ims = torch.cat([x, recon],0)
        recon_dict = {'thickness': thickness, 'intensity': intensity, 'ims': ims}
    
        torch.save(recon_dict,f=p)


    def log_img_grid(self, tag, imgs, normalize=True, save_img=False, **kwargs):
        p = os.path.join(self.trainer.logger.experiment.log_dir, f'{tag}.png')
        if save_img:
            torchvision.utils.save_image(imgs, p, normalize=normalize, **kwargs)
        
        grid = torchvision.utils.make_grid(imgs, normalize=normalize, **kwargs)
        self.logger.experiment.add_image(tag, grid, self.current_epoch)

    
    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = parent_parser.add_argument_group("LitModel")
        
        # hparams related to data
        parser.add_argument('--sample_img_interval', default=10, type=int, help="interval in which to sample and log images (default: %(default)s)")
        parser.add_argument('--train_batch_size', default=256, type=int, help="train batch size (default: %(default)s)")
        parser.add_argument('--test_batch_size', default=256, type=int, help="test batch size (default: %(default)s)")
        parser.add_argument('--val_batch_size', default=256, type=int, help="validation batch size (default: %(default)s)")
        parser.add_argument('--validate', default=None, type = int, help="whether to validate (default: %(default)s)")
        parser.add_argument('--num_workers', type=int, default=8)
        parser.add_argument('--data_dir', type=str, help="data dir (default: %(default)s)", required=True)

        # hparams related to training
        parser.add_argument('--lr', default=1e-3, type=float, help="lr of deep part (default: %(default)s)")
        parser.add_argument('--pgm_lr', default=5e-3, type=float, help="lr of pgm (default: %(default)s)")
        parser.add_argument('--l2', default=0., type=float, help="weight decay (default: %(default)s)")
        parser.add_argument('--use_amsgrad', default=False, action='store_true', help="use amsgrad? (default: %(default)s)")
        parser.add_argument('--num_sample_particles', default=32, type=int, help="number of particles to use for MC sampling (default: %(default)s)")

        # hparams related to SVI
        parser.add_argument('--pgm', default=None, help='Probabilistic graphical model (default: %(default)s',
        choices=['independent', 'conditional', 'full', 'reverse', "test"], required=True)
        parser.add_argument('--num_svi_particles', default=4, type=int, help="number of particles to use for ELBO (default: %(default)s)")
        parser.add_argument('--beta', default=1., type = float,  help="Beta parameter for beta-VAE (default: %(default)s) corresponds to normal VAE")
        parser.add_argument('--latent_dim', default=16, type=int, help="latent dimension of model (default: %(default)s)")
        parser.add_argument('--hidden_dim', default=100, type=int, help="hidden dimension of model (default: %(default)s)")
        parser.add_argument('--logstd_init', default=-5, type=float, help="init of logstd (default: %(default)s)")
        parser.add_argument('--decoder_type', default='fixed_var', help="var type (default: %(default)s)",
        choices=['fixed_var', 'learned_var', 'independent_gaussian', 'sharedvar_multivariate_gaussian', 'multivariate_gaussian',
                     'sharedvar_lowrank_multivariate_gaussian', 'lowrank_multivariate_gaussian'])
        parser.add_argument('--decoder_cov_rank', default=10, type=int, help="rank for lowrank cov approximation (requires lowrank decoder) (default: %(default)s)")  # noqa: E501
        parser.add_argument('--clip_norm', default=None, type=float, help="Parameter to clip gradients or not (default: %(default)s)")

        return parent_parser