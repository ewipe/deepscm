from typing import Mapping

import numpy as np
import pyro
import pyro.distributions as dist
import torch
from deepscm.arch.mnist import Decoder, Encoder
from deepscm.datasets.morphomnist import MorphoMNISTLike
from deepscm.distributions.transforms.affine import (
    ConditionalAffineTransform, LowerCholeskyAffine)
from deepscm.distributions.transforms.reshape import ReshapeTransform
from pyro.distributions import (LowRankMultivariateNormal, MultivariateNormal,
                                Normal, TransformedDistribution)
from pyro.distributions.conditional import ConditionalTransformedDistribution
from pyro.distributions.torch_transform import ComposeTransformModule
from pyro.distributions.transforms import (AffineTransform, ComposeTransform,
                                           ExpTransform, SigmoidTransform,
                                           Spline)
from pyro.infer import SVI, TraceGraph_ELBO
from pyro.infer.reparam.transform import TransformReparam
from pyro.nn import DenseNN, PyroModule, pyro_method
from pyro.optim import Adam
from torch.distributions import Independent


from deepscm.distributions.deep import DeepMultivariateNormal, DeepIndepNormal, Conv2dIndepNormal, DeepLowRankMultivariateNormal


class CustomELBO(TraceGraph_ELBO):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.trace_storage = {'model': None, 'guide': None}

    def _get_trace(self, model, guide, args, kwargs):
        model_trace, guide_trace = super()._get_trace(model, guide, args, kwargs)

        self.trace_storage['model'] = model_trace
        self.trace_storage['guide'] = guide_trace

        return model_trace, guide_trace

class Lambda(torch.nn.Module):
    def __init__(self, func):
        super().__init__()
        self.func = func

    def forward(self, x):
        return self.func(x)

class BaseSEM(PyroModule):
    def __init__(self, preprocessing: str = 'realnvp'):
        super().__init__()

        self.preprocessing = preprocessing

        self.register_buffer('thickness_flow_lognorm_loc', torch.zeros([], requires_grad=False))
        self.register_buffer('thickness_flow_lognorm_scale', torch.ones([], requires_grad=False))

        self.register_buffer('intensity_flow_norm_loc', torch.zeros([], requires_grad=False))
        self.register_buffer('intensity_flow_norm_scale', torch.ones([], requires_grad=False))

        self.thickness_flow_lognorm = AffineTransform(loc=self.thickness_flow_lognorm_loc.item(), scale=self.thickness_flow_lognorm_scale.item())
        self.intensity_flow_norm = AffineTransform(loc=self.intensity_flow_norm_loc.item(), scale=self.intensity_flow_norm_scale.item())

    def __setattr__(self, name, value):
        super().__setattr__(name, value)

        if name == 'thickness_flow_lognorm_loc':
            self.thickness_flow_lognorm.loc = self.thickness_flow_lognorm_loc.item()
        elif name == 'thickness_flow_lognorm_scale':
            self.thickness_flow_lognorm.scale = self.thickness_flow_lognorm_scale.item()
        elif name == 'intensity_flow_norm_loc':
            self.intensity_flow_norm.loc = self.intensity_flow_norm_loc.item()
        elif name == 'intensity_flow_norm_scale':
            self.intensity_flow_norm.scale = self.intensity_flow_norm_scale.item()

    def _get_preprocess_transforms(self):
        alpha = 0.05
        num_bits = 8

        if self.preprocessing == 'glow':
            # Map to [-0.5,0.5]
            a1 = AffineTransform(-0.5, (1. / 2 ** num_bits))
            preprocess_transform = ComposeTransform([a1])
        elif self.preprocessing == 'realnvp':
            # Map to [0,1]
            a1 = AffineTransform(0., (1. / 2 ** num_bits))

            # Map into unconstrained space as done in RealNVP
            a2 = AffineTransform(alpha, (1 - alpha))

            s = SigmoidTransform()

            preprocess_transform = ComposeTransform([a1, a2, s.inv])

        return preprocess_transform

    @pyro_method
    def pgm_scm(self, *args, **kwargs):
        def config(msg):
            if isinstance(msg['fn'], TransformedDistribution):
                return TransformReparam()
            else:
                return None

        return pyro.poutine.reparam(self.pgm_model, config=config)(*args, **kwargs)

    @pyro_method
    def scm(self, *args, **kwargs):
        def config(msg):
            if isinstance(msg['fn'], TransformedDistribution):
                return TransformReparam()
            else:
                return None

        return pyro.poutine.reparam(self.model, config=config)(*args, **kwargs)

    @pyro_method
    def sample(self, n_samples=1):
        with pyro.plate('observations', n_samples):
            samples = self.model()

        return (*samples,)

    @pyro_method
    def sample_scm(self, n_samples=1):
        with pyro.plate('observations', n_samples):
            samples = self.scm()

        return (*samples,)

    @pyro_method
    def infer_exogeneous(self, **obs):
        # assuming that we use transformed distributions for everything:
        cond_sample = pyro.condition(self.sample, data=obs)
        cond_trace = pyro.poutine.trace(cond_sample).get_trace(obs['x'].shape[0])

        output = {}
        for name, node in cond_trace.nodes.items():
            if 'fn' not in node.keys():
                continue

            fn = node['fn']
            if isinstance(fn, Independent):
                fn = fn.base_dist
            if isinstance(fn, TransformedDistribution):
                output[name + '_base'] = ComposeTransform(fn.transforms).inv(node['value'])

        return output

    @classmethod
    def add_arguments(cls, parser):
        parser.add_argument('--preprocessing', default='realnvp', type=str, help="type of preprocessing (default: %(default)s)", choices=['realnvp', 'glow'])

        return parser

class BaseVISEM(BaseSEM):
    context_dim = 0
    img_shape = (1, 28, 28)

    def __init__(self, hidden_dim: int, latent_dim: int, logstd_init: float = -5, decoder_type: str = 'fixed_var', decoder_cov_rank: int = 10, **kwargs):
        #super().__init__(**kwargs)
        super().__init__()

        self.hidden_dim = hidden_dim
        self.latent_dim = latent_dim
        self.logstd_init = logstd_init
        # TODO: This could be handled by passing a product distribution?

        # priors
        self.register_buffer('thickness_base_loc', torch.zeros([1, ], requires_grad=False))
        self.register_buffer('thickness_base_scale', torch.ones([1, ], requires_grad=False))

        self.register_buffer('intensity_base_loc', torch.zeros([1, ], requires_grad=False))
        self.register_buffer('intensity_base_scale', torch.ones([1, ], requires_grad=False))

        self.register_buffer('z_loc', torch.zeros([latent_dim, ], requires_grad=False))
        self.register_buffer('z_scale', torch.ones([latent_dim, ], requires_grad=False))

        self.register_buffer('x_base_loc', torch.zeros([1, 28, 28], requires_grad=False))
        self.register_buffer('x_base_scale', torch.ones([1, 28, 28], requires_grad=False))

        self.decoder_type = decoder_type
        self.decoder_cov_rank = decoder_cov_rank

        # decoder parts
        decoder = self.decoder = Decoder(self.latent_dim + self.context_dim)

        if self.decoder_type == 'fixed_var':
            self.decoder = Conv2dIndepNormal(decoder, 1, 1)

            torch.nn.init.zeros_(self.decoder.logvar_head.weight)
            self.decoder.logvar_head.weight.requires_grad = False

            torch.nn.init.constant_(self.decoder.logvar_head.bias, self.logstd_init)
            self.decoder.logvar_head.bias.requires_grad = False
        else:
            raise ValueError('unknown decoder type: {}'.format(self.decoder_type))

        # encoder parts
        self.encoder = Encoder(self.hidden_dim)

        # TODO: do we need to replicate the PGM here to be able to run conterfactuals? oO
        latent_layers = torch.nn.Sequential(torch.nn.Linear(self.hidden_dim + self.context_dim, self.hidden_dim), torch.nn.ReLU())
        self.latent_encoder = DeepIndepNormal(latent_layers, self.hidden_dim, self.latent_dim)

    def _get_preprocess_transforms(self):
        return super()._get_preprocess_transforms().inv

    def _get_transformed_x_dist(self, latent):
        x_pred_dist = self.decoder.predict(latent)
        x_base_dist = Normal(self.x_base_loc, self.x_base_scale).to_event(3)

        preprocess_transform = self._get_preprocess_transforms()

        x_pred_dist = x_pred_dist.base_dist
        x_reparam_transform = AffineTransform(x_pred_dist.loc, x_pred_dist.scale, 3)

        return TransformedDistribution(x_base_dist, ComposeTransform([x_reparam_transform, preprocess_transform]))

    @pyro_method
    def guide(self, x, thickness, intensity, label):
        raise NotImplementedError()

    @pyro_method
    def svi_guide(self, x, thickness, intensity, label):
        self.guide(x, thickness, intensity, label)
        
    @pyro_method
    def svi_model(self, x, thickness, intensity, label):
        with pyro.plate('observations', x.shape[0]):
            pyro.condition(self.model, data={'x': x, 'thickness': thickness, 'intensity': intensity, 'label': label})()

    @pyro_method
    def infer_z(self, *args, **kwargs):
        return self.guide(*args, **kwargs)

    @pyro_method
    def infer(self, **obs):
        _required_data = ('x', 'thickness', 'intensity', 'label')
        assert set(obs.keys()) == set(_required_data), 'got: {}'.format(tuple(obs.keys()))

        z = self.infer_z(**obs)

        exogeneous = self.infer_exogeneous(z=z, **obs)
        exogeneous['z'] = z

        return exogeneous

    @pyro_method
    def reconstruct(self, x, thickness, intensity, label, num_particles: int = 1):
        obs = {'x': x, 'thickness': thickness, 'intensity': intensity, 'label': label}
        z_dist = pyro.poutine.trace(self.guide).get_trace(**obs).nodes['z']['fn']

        recons = []
        for _ in range(num_particles):
            z = pyro.sample('z', z_dist)
            recon, *_ = pyro.poutine.condition(self.sample, data={'thickness': thickness, 'intensity': intensity, 'z': z})(x.shape[0])
            recons += [recon]
        return torch.stack(recons).mean(0)

    @pyro_method
    def counterfactual(self, obs: Mapping, condition: Mapping = None, num_particles: int = 1):
        _required_data = ('x', 'thickness', 'intensity', 'label')
        assert set(obs.keys()) == set(_required_data), 'got: {}'.format(tuple(obs.keys()))

        z_dist = pyro.poutine.trace(self.guide).get_trace(**obs).nodes['z']['fn']

        counterfactuals = []
        for _ in range(num_particles):
            z = pyro.sample('z', z_dist)

            exogeneous = self.infer_exogeneous(z=z, **obs)
            exogeneous = {k: v.detach() for k, v in exogeneous.items()}
            exogeneous['z'] = z
            counter = pyro.poutine.do(pyro.poutine.condition(self.sample_scm, data=exogeneous), data=condition)(obs['x'].shape[0])
            counterfactuals += [counter]
        return {k: v for k, v in zip(('x', 'z', 'thickness', 'intensity'), (torch.stack(c).mean(0) for c in zip(*counterfactuals)))}

class ConditionalVISEM(BaseVISEM):
    context_dim = 12

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Flow for modelling t Gamma
        self.thickness_flow_components = ComposeTransformModule([Spline(1)])
        self.thickness_flow_constraint_transforms = ComposeTransform([self.thickness_flow_lognorm, ExpTransform()])
        self.thickness_flow_transforms = ComposeTransform([self.thickness_flow_components, self.thickness_flow_constraint_transforms])

        # affine flow for s normal
        intensity_net = DenseNN(1, [1], param_dims=[1, 1], nonlinearity=torch.nn.Identity())
        self.intensity_flow_components = ConditionalAffineTransform(context_nn=intensity_net, event_dim=0)
        self.intensity_flow_constraint_transforms = ComposeTransform([SigmoidTransform(), self.intensity_flow_norm])
        self.intensity_flow_transforms = [self.intensity_flow_components, self.intensity_flow_constraint_transforms]

    @pyro_method
    def pgm_model(self):
        thickness_base_dist = Normal(self.thickness_base_loc, self.thickness_base_scale).to_event(1)
        thickness_dist = TransformedDistribution(thickness_base_dist, self.thickness_flow_transforms)

        thickness = pyro.sample('thickness', thickness_dist)
        thickness_ = self.thickness_flow_constraint_transforms.inv(thickness)
        # pseudo call to thickness_flow_transforms to register with pyro
        _ = self.thickness_flow_components

        intensity_base_dist = Normal(self.intensity_base_loc, self.intensity_base_scale).to_event(1)
        intensity_dist = ConditionalTransformedDistribution(intensity_base_dist, self.intensity_flow_transforms).condition(thickness_)

        intensity = pyro.sample('intensity', intensity_dist)
        # pseudo call to intensity_flow_transforms to register with pyro
        _ = self.intensity_flow_components

        label_dist = dist.Categorical(self.cat_prior).to_event(0)

        label = pyro.sample('label', label_dist)

        return thickness, intensity, label

    @pyro_method
    def model(self):
        thickness, intensity, label = self.pgm_model()

        thickness_ = self.thickness_flow_constraint_transforms.inv(thickness)
        intensity_ = self.intensity_flow_norm.inv(intensity)

        # transform one_hot to categorical
        label_ = torch.nn.functional.one_hot(label.to(torch.int64),num_classes=10)

        z_loc = self.z_loc
        z_scale = self.z_scale
        with pyro.poutine.scale(scale=self.beta):
            z = pyro.sample('z', Normal(z_loc, z_scale).to_event(1))

        latent = torch.cat([z, thickness_, intensity_, label_], 1)
        x_dist = self._get_transformed_x_dist(latent)

        x = pyro.sample('x', x_dist)

        return x, z, thickness, intensity, label

    @pyro_method
    def guide(self, x, thickness, intensity, label):
        with pyro.plate('observations', x.shape[0]):
            hidden = self.encoder(x)

            thickness_ = self.thickness_flow_constraint_transforms.inv(thickness)
            intensity_ = self.intensity_flow_norm.inv(intensity)
            label_ = torch.nn.functional.one_hot(label.to(torch.int64),num_classes=10)

            hidden = torch.cat([hidden, thickness_, intensity_, label_], 1)
            latent_dist = self.latent_encoder.predict(hidden)

            # make it a beta VAE
            with pyro.poutine.scale(scale=self.beta):
                z = pyro.sample('z', latent_dist)

        return z

    @pyro_method
    def infer_thickness_base(self, thickness):
        return self.thickness_flow_transforms.inv(thickness)

    @pyro_method
    def infer_intensity_base(self, thickness, intensity):
        intensity_base_dist = Normal(self.intensity_base_loc, self.intensity_base_scale)
        cond_intensity_transforms = ComposeTransform(
            ConditionalTransformedDistribution(intensity_base_dist, self.intensity_flow_transforms).condition(thickness).transforms)
        return cond_intensity_transforms.inv(intensity)
