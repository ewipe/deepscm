from argparse import Namespace
import pyro

from pyro.nn import PyroModule, pyro_method

from pyro.distributions import TransformedDistribution
from pyro.infer.reparam.transform import TransformReparam
from torch.distributions import Independent

from deepscm.datasets.morphomnist import MorphoMNISTLike
from pyro.distributions.transforms import ComposeTransform, SigmoidTransform, AffineTransform

import torchvision.utils
from torch.utils.data import DataLoader, random_split
import pytorch_lightning as pl
import torch
import numpy as np

import seaborn as sns
import matplotlib.pyplot as plt

from deepscm.morphomnist import measure
import os
from functools import partial
import multiprocessing

from deepscm.experiments.digit.pyro_model import ConditionalVISEM as pyro_model

from deepscm.distributions.deep import DeepMultivariateNormal, DeepIndepNormal, Conv2dIndepNormal, DeepLowRankMultivariateNormal
from pyro.infer import SVI, TraceGraph_ELBO
from pyro.optim import Adam



class CustomELBO(TraceGraph_ELBO):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.trace_storage = {'model': None, 'guide': None}

    def _get_trace(self, model, guide, args, kwargs):
        model_trace, guide_trace = super()._get_trace(model, guide, args, kwargs)

        self.trace_storage['model'] = model_trace
        self.trace_storage['guide'] = guide_trace

        return model_trace, guide_trace

class Lambda(torch.nn.Module):
    def __init__(self, func):
        super().__init__()
        self.func = func

    def forward(self, x):
        return self.func(x)


class Experiment(pl.LightningModule):
    def __init__(self, **kwargs):
        super().__init__()

        # Initiate kwarg key value pairs as methods
        for key, value in kwargs.items():
            setattr(self, key, value)

        # save hyper parameters
        self.save_hyperparameters()

        # Initiate pyro model parameters
        self.pyro_model = pyro_model(**kwargs)

        ## add beta for beta VAE
        self.pyro_model.beta = self.hparams.beta

        self.svi_loss = CustomELBO(num_particles=self.hparams.num_svi_particles)
    
        if self.num_sample_particles:
            self.pyro_model._gen_counterfactual = partial(self.pyro_model.counterfactual, num_particles=self.num_sample_particles)
        else:
            self.pyro_model._gen_counterfactual = self.pyro_model.counterfactual

        if self.validate:
            import random

            torch.manual_seed(0)
            np.random.seed(0)
            random.seed(0)

            torch.backends.cudnn.deterministic = True
            torch.backends.cudnn.benchmark = False
            torch.autograd.set_detect_anomaly(self.hparams.validate)
            pyro.enable_validation()
        
        # init svi loss
        self._build_svi()
    
    def _build_svi(self, loss=None):
        def per_param_callable(module_name, param_name):
            params = {'eps': 1e-5, 'amsgrad': self.hparams.use_amsgrad, 'weight_decay': self.hparams.l2}
            if module_name == 'intensity_flow_components' or module_name == 'thickness_flow_components':
                params['lr'] = self.hparams.pgm_lr
                return params
            else:
                params['lr'] = self.hparams.lr
                return params

        if loss is None:
            loss = self.svi_loss
        
        self.svi = SVI(self.pyro_model.svi_model, self.pyro_model.svi_guide, Adam(per_param_callable), loss)
        self.svi.loss_class = loss

    def prepare_data(self):
        # prepare transforms standard to MNIST
        mnist_train = MorphoMNISTLike(self.data_dir, train=True, columns=['thickness', 'intensity'])
        self.mnist_test = MorphoMNISTLike(self.data_dir, train=False, columns=['thickness', 'intensity'])

        num_val = int(len(mnist_train) * 0.1)
        num_train = len(mnist_train) - num_val
        self.mnist_train, self.mnist_val = random_split(mnist_train, [num_train, num_val])
        
        ## There is a glitch so self.device is not set correctly - we have to do it this way

        self.cuda_device = self.trainer.strategy.root_device
        
        print(f'using device: {self.cuda_device}')

        thicknesses = 1. + torch.arange(3, dtype=torch.float, device=self.cuda_device)
        self.thickness_range = thicknesses.repeat(3).unsqueeze(1)
        intensity = 48 * torch.arange(3, dtype=torch.float, device=self.cuda_device) + 64
        self.intensity_range = intensity.repeat_interleave(3).unsqueeze(1)
        self.z_range = torch.randn([1, self.hparams.latent_dim], device=self.cuda_device, dtype=torch.float).repeat((9, 1))

        self.pyro_model.intensity_flow_norm_loc = mnist_train.metrics['intensity'].min().to(self.cuda_device).float()
        self.pyro_model.intensity_flow_norm_scale = (mnist_train.metrics['intensity'].max() - mnist_train.metrics['intensity'].min()).to(self.cuda_device).float()  # noqa: E501

        self.pyro_model.thickness_flow_lognorm_loc = mnist_train.metrics['thickness'].log().mean().to(self.cuda_device).float()
        self.pyro_model.thickness_flow_lognorm_scale = mnist_train.metrics['thickness'].log().std().to(self.cuda_device).float()

        #prior for device
        self.pyro_model.cat_prior = torch.ones(10).to(self.cuda_device)

        print(f'set thickness_flow_lognorm.loc to {self.pyro_model.thickness_flow_lognorm.loc}')

    def configure_optimizers(self):
        # We use Pyros internal optimizers
        pass

    def train_dataloader(self):
        return DataLoader(self.mnist_train, batch_size=self.train_batch_size, shuffle=True, num_workers=self.num_workers)

    def val_dataloader(self):
        self.val_loader = DataLoader(self.mnist_test, batch_size=self.val_batch_size, shuffle=False, num_workers = self.num_workers)
        return self.val_loader

    def test_dataloader(self):
        self.test_loader = DataLoader(self.mnist_test, batch_size=self.test_batch_size, shuffle=False, num_workers = self.num_workers)
        return self.test_loader

    def forward(self, *args, **kwargs):
        pass

    def validation_epoch_end(self, outputs):
        outputs = self.assemble_epoch_end_outputs(outputs)

        metrics = {('val/' + k): v for k, v in outputs.items()}
        if self.current_epoch % self.hparams.sample_img_interval == 0:
             self.sample_images()

        self.log_dict(metrics)
    
    def assemble_output(self, outputs):

        outputs_ = Namespace() # we like namespace rather than dict
        
        # unpack here
        for key in outputs[0].keys():
            _ = []

            for item in outputs:
                _.append(item[key])

            merged = torch.concat(_)
            outputs_.__setattr__(key, merged)

        return outputs_

    def assemble_epoch_end_outputs(self, outputs):
        num_items = len(outputs)

        def handle_row(batch, assembled=None):
            if assembled is None:
                assembled = {}

            for k, v in batch.items():
                if k not in assembled.keys():
                    if isinstance(v, dict):
                        assembled[k] = handle_row(v)
                    elif isinstance(v, float):
                        assembled[k] = v
                    elif np.prod(v.shape) == 1:
                        assembled[k] = v.cpu()
                    else:
                        assembled[k] = v.cpu()
                else:
                    if isinstance(v, dict):
                        assembled[k] = handle_row(v, assembled[k])
                    elif isinstance(v, float):
                        assembled[k] += v
                    elif isinstance(v, list):
                        assembled[k].append(v)
                    elif np.prod(v.shape) == 1:
                        assembled[k] += v.cpu()
                    else:
                        assembled[k] = torch.cat([assembled[k], v.cpu()], 0)

            return assembled

        assembled = {}
        for _, batch in enumerate(outputs):
            assembled = handle_row(batch, assembled)

        for k, v in assembled.items():
            if (hasattr(v, 'shape') and np.prod(v.shape) == 1) or isinstance(v, float):
                assembled[k] /= num_items

        return assembled

    def log_img_grid(self, tag, imgs, normalize=True, save_img=False, **kwargs):
        if save_img:
            p = os.path.join(self.trainer.default_root_dir, f'{tag}.png')
            torchvision.utils.save_image(imgs, p,**kwargs)
        grid = torchvision.utils.make_grid(imgs, normalize=normalize,**kwargs)
        self.logger.experiment.add_image(tag, grid, self.current_epoch)
    
    def move_to_device(self, batch):
        batch = self.trainer.strategy.batch_to_device(batch)
        return batch
    
    def get_batch(self, loader):
        batch = next(iter(loader))
        batch = self.move_to_device(batch)
        return batch

    def build_reconstruction(self, x, thickness, intensity, label, tag='reconstruction', **kwargs):
        obs = {'x': x, 'thickness': thickness, 'intensity': intensity, 'label': label}

        recon = self.pyro_model.reconstruct(**obs, num_particles=self.hparams.num_sample_particles)
        self.log_img_grid(tag, torch.cat([x, recon], 0), **kwargs)
        self.logger.experiment.add_scalar(f'{tag}/mse', torch.mean(torch.square(x - recon).sum((1, 2, 3))), self.current_epoch)

    def build_counterfactual(self,obs,conditions, absolute=None, log_img_grid=False, tag=None, nrow = None, **kwargs):
        _required_data = ('x', 'thickness', 'intensity', 'label')
        assert set(obs.keys()) == set(_required_data), 'got: {}'.format(tuple(obs.keys()))

        imgs = [obs['x']]

        for name, data in conditions.items():
            counterfactual = self.pyro_model._gen_counterfactual(obs=obs, condition=data)

            counter = counterfactual['x']
            imgs.append(counter)
        
        if log_img_grid:
            self.log_img_grid(tag, torch.cat(imgs, 0), nrow=nrow)

        return torch.stack(imgs,axis=1)

    def sample_images_test(self, type='test/', loader=None):

        if loader == None:
            loader = self.test_loader
    
        with torch.no_grad():
            sample_trace = pyro.poutine.trace(self.pyro_model.sample).get_trace(1000) # take 1000 samples

            #### Sample

            # flip to obtain same observations for different batch_sizes
            samples = sample_trace.nodes['x']['value'].flip(0)
            sampled_label = sample_trace.nodes['label']['value'].flip(0)

            #obtain first indice fo each digit
            unique, idx, counts = torch.unique(sampled_label, sorted=True, return_inverse=True, return_counts=True)
            _, ind_sorted = torch.sort(idx)
            cum_sum = counts.cumsum(0)
            cum_sum = torch.cat((cum_sum.new([0]),cum_sum[:-1]))
            first_indicies = ind_sorted[cum_sum]

            self.log_img_grid(f'{type}samples', samples.data[first_indicies], nrow=len(first_indicies))

            ### Visualize counterfactuals
            # obtain batch
            all_data = self.prep_batch((loader.dataset[:]))

            #obtain first indice fo each digit
            label = all_data['label']
            unique, idx, counts = torch.unique(label, sorted=True, return_inverse=True, return_counts=True)
            _, ind_sorted = torch.sort(idx)
            cum_sum = counts.cumsum(0)
            cum_sum = torch.cat((cum_sum.new([0]),cum_sum[:-1]))
            obs_batch_indicies = ind_sorted[cum_sum]

            obs_batch = {k: v[obs_batch_indicies] for k, v in all_data.items()}

            self.log_img_grid(f'{type}input', obs_batch['x'], save_img=False, nrow=len(first_indicies))

            if hasattr(self.pyro_model, 'reconstruct'):
                self.build_reconstruction(**obs_batch,  nrow=len(first_indicies), tag=f'{type}reconstruct')

            conditions = {
                '+1': {'thickness': obs_batch['thickness'] + 1},
                '+2': {'thickness': obs_batch['thickness'] + 2},
                '+3': {'thickness': obs_batch['thickness'] + 3}
            }
            self.build_counterfactual(tag=f'{type}do(thickness=+x)', obs=obs_batch, conditions=conditions, nrow=len(first_indicies), log_img_grid=True)

            conditions = {
                '-64': {'intensity': obs_batch['intensity'] - 64},
                '-32': {'intensity': obs_batch['intensity'] - 32},
                '+32': {'intensity': obs_batch['intensity'] + 32},
                '+64': {'intensity': obs_batch['intensity'] + 64}
            }
            self.build_counterfactual(tag=f'{type}do(intensity=+x)', obs=obs_batch, conditions=conditions, nrow=len(first_indicies), log_img_grid=True)

            #add counterfactuals for digit type
            conditions = {
                '0': {'label': torch.ones_like(obs_batch['label']) * 0},
                '1': {'label': torch.ones_like(obs_batch['label']) * 1},
                '2': {'label': torch.ones_like(obs_batch['label']) * 2},
                '3': {'label': torch.ones_like(obs_batch['label']) * 3},
                '4': {'label': torch.ones_like(obs_batch['label']) * 4},
                '5': {'label': torch.ones_like(obs_batch['label']) * 5},
                '6': {'label': torch.ones_like(obs_batch['label']) * 6},
                '7': {'label': torch.ones_like(obs_batch['label']) * 7},
                '8': {'label': torch.ones_like(obs_batch['label']) * 8},
                '9': {'label': torch.ones_like(obs_batch['label']) * 9},
            }
            self.build_counterfactual(tag=f'{type}do(label=x)', obs=obs_batch, conditions=conditions, nrow=len(first_indicies), log_img_grid=True)


    def sample_images(self):

        ## TODO do this in a way more orderly fashion... i.e do everything in testing
        with torch.no_grad():
            sample_trace = pyro.poutine.trace(self.pyro_model.sample).get_trace(self.hparams.test_batch_size)

            # flip to obtain same observations for different batch_sizes
            samples = sample_trace.nodes['x']['value'].flip(0)
            sampled_label = sample_trace.nodes['label']['value'].flip(0)

            #obtain first indice fo each digit
            unique, idx, counts = torch.unique(sampled_label, sorted=True, return_inverse=True, return_counts=True)
            _, ind_sorted = torch.sort(idx)
            cum_sum = counts.cumsum(0)
            cum_sum = torch.cat((cum_sum.new([0]),cum_sum[:-1]))
            first_indicies = ind_sorted[cum_sum]

            self.log_img_grid('samples', samples.data[first_indicies], nrow=len(first_indicies))

            cond_data = {'thickness': self.thickness_range, 'intensity': self.intensity_range, 'z': self.z_range}
            samples, *_ = pyro.condition(self.pyro_model.sample, data=cond_data)(9)
            self.log_img_grid('cond_samples', samples.data, nrow=3)

            obs_batch = self.prep_batch(self.get_batch(self.val_loader))

            exogeneous = self.pyro_model.infer(**obs_batch)
            for (tag, val) in exogeneous.items():
                self.logger.experiment.add_histogram(tag, val, self.current_epoch)
            
            #obtain first indice fo each digit
            batch_label = obs_batch['label']
            unique, idx, counts = torch.unique(batch_label, sorted=True, return_inverse=True, return_counts=True)
            _, ind_sorted = torch.sort(idx)
            cum_sum = counts.cumsum(0)
            cum_sum = torch.cat((cum_sum.new([0]),cum_sum[:-1]))
            obs_batch_indicies = ind_sorted[cum_sum]

            obs_batch = {k: v[obs_batch_indicies] for k, v in obs_batch.items()}

            self.log_img_grid('input', obs_batch['x'], save_img=False, nrow=len(first_indicies))

            if hasattr(self.pyro_model, 'reconstruct'):
                self.build_reconstruction(**obs_batch,  nrow=len(first_indicies))

            conditions = {
                '+1': {'thickness': obs_batch['thickness'] + 1},
                '+2': {'thickness': obs_batch['thickness'] + 2},
                '+3': {'thickness': obs_batch['thickness'] + 3}
            }
            self.build_counterfactual(tag='do(thickness=+x)', obs=obs_batch, conditions=conditions, nrow=len(first_indicies), log_img_grid=True)

            conditions = {
                '-64': {'intensity': obs_batch['intensity'] - 64},
                '-32': {'intensity': obs_batch['intensity'] - 32},
                '+32': {'intensity': obs_batch['intensity'] + 32},
                '+64': {'intensity': obs_batch['intensity'] + 64}
            }
            self.build_counterfactual(tag='do(intensity=+x)', obs=obs_batch, conditions=conditions, nrow=len(first_indicies), log_img_grid=True)

            #add counterfactuals for digit type
            conditions = {
                '0': {'label': torch.ones_like(obs_batch['label']) * 0},
                '1': {'label': torch.ones_like(obs_batch['label']) * 1},
                '2': {'label': torch.ones_like(obs_batch['label']) * 2},
                '3': {'label': torch.ones_like(obs_batch['label']) * 3},
                '4': {'label': torch.ones_like(obs_batch['label']) * 4},
                '5': {'label': torch.ones_like(obs_batch['label']) * 5},
                '6': {'label': torch.ones_like(obs_batch['label']) * 6},
                '7': {'label': torch.ones_like(obs_batch['label']) * 7},
                '8': {'label': torch.ones_like(obs_batch['label']) * 8},
                '9': {'label': torch.ones_like(obs_batch['label']) * 9},
            }
            self.build_counterfactual(tag='do(label=x)', obs=obs_batch, conditions=conditions, nrow=len(first_indicies), log_img_grid=True)

    def backward(self, *args, **kwargs):
        pass  # No loss to backpropagate since we're using Pyro's optimisation machinery

    def get_trace_metrics(self, batch):
        metrics = {}

        model = self.svi.loss_class.trace_storage['model']
        guide = self.svi.loss_class.trace_storage['guide']

        metrics['log p(x)'] = model.nodes['x']['log_prob'].mean()
        metrics['log p(intensity)'] = model.nodes['intensity']['log_prob'].mean()
        metrics['log p(thickness)'] = model.nodes['thickness']['log_prob'].mean()
        metrics['log p(label)'] = model.nodes['label']['log_prob'].mean()

        metrics['p(z)'] = model.nodes['z']['log_prob'].mean()
        metrics['q(z)'] = guide.nodes['z']['log_prob'].mean()
        metrics['log p(z) - log q(z)'] = metrics['p(z)'] - metrics['q(z)']

        return metrics

    def prep_batch(self, batch):
        batch = self.trainer.strategy.batch_to_device(batch) # move to correct device

        x = batch['image']
        thickness = batch['thickness'].unsqueeze(1).float()
        intensity = batch['intensity'].unsqueeze(1).float()
        label = batch['label']
        
        x = x.float()

        if self.training:
            x += torch.rand_like(x)

        x = x.unsqueeze(1)

        return {'x': x, 'thickness': thickness, 'intensity': intensity, 'label': label}

    def training_step(self, batch, batch_idx):
        batch = self.prep_batch(batch)

        loss = self.svi.step(**batch)

        metrics = self.get_trace_metrics(batch)

        if np.isnan(loss):
            self.logger.experiment.add_text('nan', f'nand at {self.current_epoch}:\n{metrics}')
            raise ValueError('loss went to nan with metrics:\n{}'.format(metrics))

        tensorboard_logs = {('train/' + k): v for k, v in metrics.items()}
        tensorboard_logs['train/loss'] = loss

        self.log_dict(tensorboard_logs)

        return torch.Tensor([loss])

    def validation_step(self, batch, batch_idx):
        batch = self.prep_batch(batch)

        loss = self.svi.evaluate_loss(**batch)

        metrics = self.get_trace_metrics(batch)

        return {'loss': loss, **metrics}
    
    def test_step(self, batch, batch_ix):
        batch = self.prep_batch(batch)

        x, thickness, intensity, label = batch.values()

        # make counterfactuals

        ## label
        one_tensor = torch.ones_like(batch['label'])
        conditions = {
            '0': {'label': one_tensor * 0},
            '1': {'label': one_tensor * 1},
            '2': {'label': one_tensor * 2},
            '3': {'label': one_tensor * 3},
            '4': {'label': one_tensor * 4},
            '5': {'label': one_tensor * 5},
            '6': {'label': one_tensor * 6},
            '7': {'label': one_tensor * 7},
            '8': {'label': one_tensor * 8},
            '9': {'label': one_tensor * 9},
        }
        do_label = self.build_counterfactual(tag='do(label=x)', obs=batch, conditions=conditions, log_img_grid=False)

        ## thickness
        conditions = {
                '+1': {'thickness': thickness + 1},
                '+2': {'thickness': thickness + 2},
                '+3': {'thickness': thickness + 3}
            }
        do_thickness = self.build_counterfactual(tag='do(thickness=+x)', obs=batch, conditions=conditions,log_img_grid=False)

        ## intensity
        conditions = {
            '-64': {'intensity': intensity - 64},
            '-32': {'intensity': intensity - 32},
            '+32': {'intensity': intensity + 32},
            '+64': {'intensity': intensity + 64}
        }
        do_intensity = self.build_counterfactual(tag='do(intensity=+x)', obs=batch, conditions=conditions, log_img_grid= False)

        ## return output
        return {'do_label': do_label,
                'do_thickness': do_thickness,
                'do_intensity': do_intensity,
                'label': label,
                'x': x,
                'thickness': thickness,
                'intensity': intensity
                }
    
    def on_test_start(self):
        pass
    
    def test_epoch_end(self, outputs):
        output = outputs[0] # for debugging

        # quick sanity test for tensorboard
        self.sample_images_test()

        # save output for figure
        outputs_ = self.assemble_output(outputs)
        
        #p = os.path.join(self.trainer.log_dir, 'outputs.pt')
        #torch.save(outputs_, p)

        self.outputs = outputs_

        pass

    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = parent_parser.add_argument_group("LitModel")
        
        # hparams related to data
        parser.add_argument('--sample_img_interval', default=10, type=int, help="interval in which to sample and log images (default: %(default)s)")
        parser.add_argument('--train_batch_size', default=256, type=int, help="train batch size (default: %(default)s)")
        parser.add_argument('--test_batch_size', default=256, type=int, help="test batch size (default: %(default)s)")
        parser.add_argument('--val_batch_size', default=256, type=int, help="validation batch size (default: %(default)s)")
        parser.add_argument('--validate', default=False, action='store_true', help="whether to validate (default: %(default)s)")
        parser.add_argument('--num_workers', type=int, default=min(eval(f"{os.cpu_count()}"), 16)) # set a maximum of 16 workers
        parser.add_argument('--data_dir', default="assets/data/morphomnist", type=str, help="data dir (default: %(default)s)")

        # hparams related to training
        parser.add_argument('--lr', default=1e-3, type=float, help="lr of deep part (default: %(default)s)")
        parser.add_argument('--pgm_lr', default=5e-3, type=float, help="lr of pgm (default: %(default)s)")
        parser.add_argument('--l2', default=0., type=float, help="weight decay (default: %(default)s)")
        parser.add_argument('--use_amsgrad', default=False, action='store_true', help="use amsgrad? (default: %(default)s)")
        parser.add_argument('--num_sample_particles', default=32, type=int, help="number of particles to use for MC sampling (default: %(default)s)")

        # hparams related to SVI
        parser.add_argument('--pgm', default="full", type=str, help="pgm structure used (default: %(default)s)")
        parser.add_argument('--num_svi_particles', default=4, type=int, help="number of particles to use for ELBO (default: %(default)s)")
        parser.add_argument('--beta', default=1., type = float,  help="Beta parameter for beta-VAE (default: %(default)s) corresponds to normal VAE")
        parser.add_argument('--latent_dim', default=16, type=int, help="latent dimension of model (default: %(default)s)")
        parser.add_argument('--hidden_dim', default=100, type=int, help="hidden dimension of model (default: %(default)s)")
        parser.add_argument('--logstd_init', default=-5, type=float, help="init of logstd (default: %(default)s)")
        parser.add_argument('--decoder_type', default='fixed_var', help="var type (default: %(default)s)",
        choices=['fixed_var', 'learned_var', 'independent_gaussian', 'sharedvar_multivariate_gaussian', 'multivariate_gaussian',
                     'sharedvar_lowrank_multivariate_gaussian', 'lowrank_multivariate_gaussian'])
        parser.add_argument('--decoder_cov_rank', default=10, type=int, help="rank for lowrank cov approximation (requires lowrank decoder) (default: %(default)s)")  # noqa: E501

        return parent_parser